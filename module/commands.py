from module.app import app, db
import click
import yaml
from hashlib import sha256
# Import des modèles
from module.models import Artist, Music, Genre, User, Productor, load_user, Appartient, Theme

@app.cli.command()
@click.argument('filename')
def loaddb(filename):
	'''Cree les tables et les remplit avec les données'''

	# Création de toutes les tables
	db.create_all()
	morceaux = yaml.load(open(filename))
	productors = {}
	artists = {}
	genres = {}
	for m in morceaux:
		a = m["parent"].capitalize()
		if a not in productors:
			o = Productor(name=a)
			# On ajoute l'objet o à la Base :
			db.session.add(o)
			productors[a]=o
		b = m["by"]
		if b not in artists:
			o = Artist(name=b)
			db.session.add(o)
			artists[b]=o
		for c in m["genre"]:
			g = c.capitalize()
			if "." in g:
				g = g.replace(".", " ")
			if "-" in g:
				g = g.replace("-", " ")
			if g not in genres:
				o = Genre(name=g)
				print("Genre:", g)
				db.session.add(o)
				genres[g]=o
	# On dit à la DB d'intégrer toutes les nouvelles données:
	db.session.commit()

	# Création des musiques
	for m in morceaux:
		p = productors[m["parent"].capitalize()]
		a = artists[m["by"]]
		musique = Music(
			id				= m["entryId"],
			title			= m["title"].capitalize(),
			img				= m["img"],
			releaseYear		= m["releaseYear"],
			artist_id		= a.id,
			productor_id	= p.id
		)
		print(musique)
		# On ajoute l'objet o à la Base :
		db.session.add(musique)
		# Création du lien entre les musiques et les genres.
		for g in m["genre"]:
			g = g.capitalize()
			if "." in g:
				g = g.replace(".", " ")
			if "-" in g:
				g = g.replace("-", " ")
			asso = Appartient(
				music_id	= m["entryId"],
				genre_id	= genres[g].id
			)
			db.session.add(asso)
	db.session.commit()

	# Création des themes
	liste_theme = [
		"burlywood",
		"chocolate",
		"crimson",
		"deeppink",
		"blueviolet",
		"cornflowerblue",
		"limegreen"
	]
	for couleur in liste_theme:
		theme = Theme(name=couleur)
		db.session.add(theme)
	db.session.commit()


@app.cli.command()
def syncdb():
	'''Creates all missing tables'''
	db.create_all()


@app.cli.command()
@click.argument('username')
@click.argument('password')
def newuser(username, password):
	'''Adds a new user'''
	if load_user(username) is not None:
		print('Utilisateur existant ! \nUtilisez la commande passwd pour modifier son passwd', username)
	else:
		m = sha256()
		m.update(password.encode())
		u = User(username=username, password=m.hexdigest(), theme_id=1)
		db.session.add(u)
		db.session.commit()


@app.cli.command()
def passwd(username, password):
	'''Change a user password'''
	m = sha256()
	m.update(password.encode())
	u=load_user(username)
	u.password=m.hexdigest()
	db.session.commit()
