// Déclaration des variables
let btn_supprimer = document.getElementsByClassName("btn-supprimer")[0]
let btn_close = document.getElementsByClassName("close")[0]
let filtre = document.getElementsByClassName("modal-filtre")[0]


// Définition des actions
show = function(){
    filtre.classList.remove("hidden");
}

hidden = function(){
    filtre.classList.add("hidden");
}


// Ajout des eventListener
btn_supprimer.addEventListener("click", show);
btn_close.addEventListener("click", hidden)
filtre.addEventListener("click", hidden)