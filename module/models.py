from flask_login import UserMixin
from module.app import db, login_manager
from flask_wtf import FlaskForm
from hashlib import sha256
from wtforms import StringField, HiddenField, PasswordField, validators


class Appartient(db.Model):
        id              = db.Column(db.Integer, primary_key=True, autoincrement=True)
        music_id        = db.Column(db.Integer, db.ForeignKey('music.id'))
        genre_id        = db.Column(db.Integer, db.ForeignKey('genre.id'))
        music           = db.relationship("Music", backref=db.backref("appartient", cascade="all, delete-orphan"))
        genre           = db.relationship("Genre", backref=db.backref("appartient", cascade="all, delete-orphan"))
        def __repr__(self):
                return "<Appartient (%d, %d)>" % (self.music_id, self.genre_id)

class Music(db.Model):
        id              = db.Column(db.Integer, primary_key=True)
        title           = db.Column(db.String(120))
        img             = db.Column(db.String(256))
        releaseYear     = db.Column(db.Integer)
        artist_id       = db.Column(db.Integer, db.ForeignKey("artist.id"))
        artist          = db.relationship("Artist", backref=db.backref("musics", lazy="dynamic"))
        productor_id    = db.Column(db.Integer, db.ForeignKey("productor.id"))
        productor       = db.relationship("Productor", backref=db.backref("musics", lazy="dynamic"))
        genres          = db.relationship("Genre", secondary="appartient")
        def __repr__(self):
                return "<Music (%d) %s>" % (self.id, self.title)


class Artist(db.Model):
        id      = db.Column(db.Integer, primary_key=True, autoincrement=True)
        name    = db.Column(db.String(100))
        music   = db.relationship("Music", back_populates="artist")
        def __repr__(self):
                return "<Artist (%d) %s>" % (self.id, self.name)


class Productor(db.Model):
        id      = db.Column(db.Integer, primary_key=True, autoincrement=True)
        name    = db.Column(db.String(100))
        music   = db.relationship("Music", back_populates="productor")
        def __repr__(self):
                return "<Productor (%d) %s>" % (self.id, self.name)


class Genre(db.Model):
        id      = db.Column(db.Integer, primary_key=True, autoincrement=True)
        name    = db.Column(db.String(70))
        music   = db.relationship("Music", secondary="appartient")
        def __repr__(self):
                return "<Genre (%d) %s>" % (self.id, self.name)


class User(db.Model, UserMixin):
        username        = db.Column(db.String(50), primary_key=True)
        password        = db.Column(db.String(64))
        theme_id        = db.Column(db.Integer, db.ForeignKey("theme.id"))
        theme           = db.relationship("Theme", backref=db.backref("User", lazy="dynamic"))
        def get_id(self):
                return self.username
        
        def set_theme(self, id, username):
                theme = Theme.query.get(id)
                self.theme_id = id
                db.session.commit()
                return theme.name

class Theme(db.Model):
        id = db.Column(db.Integer, primary_key=True, autoincrement=True)
        name = db.Column(db.String(20))
        user = db.relationship("User", back_populates="theme")

        def __repr__(self):
                return "<Theme (%d) %s>" % (self.id, self.name)

class LoginForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')

    def get_authenticated_user(self):
        user = User.query.get(self.username.data)
        if user is None:
            return None
        m = sha256()
        m.update(self.password.data.encode())
        passwd = m.hexdigest()
        return user if passwd == user.password else None


class RegistrationForm(FlaskForm):
    username = StringField('Username',[validators.Required()])
    password = PasswordField(
        'Password', [validators.Required(), validators.EqualTo('comfPassword')])
    comfPassword = PasswordField('Repeat Password')
    def registr (self):
           m = sha256()
           m.update(self.password.data.encode())
           us = User(username=self.username.data, password=m.hexdigest(), theme_id=1)
           db.session.add(us)
           db.session.commit()


class EditForm(FlaskForm):
        id = StringField("id")
        titre = StringField("titre",[validators.Required()])
        artiste = StringField("artiste",[validators.Required()])
        producteur = StringField("producteur",[validators.Required()])
        genre = StringField("genre",[validators.Required()])
        annee = StringField("annee",[validators.Required()])


class EditFormUser(FlaskForm):
        id = StringField("id")
        username = StringField("username",[validators.Required()])
        password = PasswordField("password",[validators.Required()])

class DeleteForm(FlaskForm):
        id = StringField("id")

#### GETTEURS ###############
def get_sample(debut=0, nb=10):
        return Music.query.offset(debut).limit(nb).all()

def get_all():
        return Music.query.order_by(Music.title).all()


def get_all_user():
        return User.query.order_by(User.username).all()

def get_author(id):
        return Artist.query.get(id)
        # ou return Author.query.fil

def get_musique_by_id(id):
        musique = Music.query.get(id)
        appartient = Appartient.query.filter_by(music_id=id).all()
        genres_name = []
        for association in appartient:
                genres_name.append(Genre.query.get(association.genre_id).name.capitalize())
        return (musique, genres_name)


def get_user(id):
        user = User.query.get(id)
        return user

@login_manager.user_loader
def load_user(username):
        return User.query.get(username)


def delete_user(form):
        user = User.query.get(form.id.data)
        db.session.delete(user)
        db.session.commit()

def delete_musique(form):
        musique = Music.query.get(int(form.id.data))
        db.session.delete(musique)
        db.session.commit()


def new_musique(form):
        last_id = Music.query.all()[-1].id
        form.id.data = last_id
        musique = Music(
                id              = last_id + 1,
                title           = form.titre.data.capitalize(),
                artist_id       = -1,
                productor_id    = -1,
                releaseYear     = int(form.annee.data)
        )
        db.session.add(musique)
        db.session.commit()
        edit_musique(form, musique)


def update_musique(form):
        # Modification des valeurs de la musique
        musique = Music.query.get(int(form.id.data))
        musique.title = form.titre.data
        musique.releaseYear = int(form.annee.data)
        edit_musique(form, musique)


def update_user(form):
        # Modification des valeurs de la musique
        user = User.query.get(form.id.data)
        m = sha256()
        m.update(form.password.data.encode())
        user.password = m.hexdigest()
        db.session.commit()


def edit_musique(form, musique):
        form_music_id = int(form.id.data)
        form_music_titre = form.titre.data
        form_music_annee = int(form.annee.data)
        form_artiste = form.artiste.data
        form_producteur = form.producteur.data
        form_genres = form.genre.data.split(", ")
        # Modification des informations de l'artiste
        if not artiste_existe(form_artiste):
                artiste = Artist(name=form_artiste)
                db.session.add(artiste)
        musique.artist_id = Artist.query.filter_by(name=form_artiste).first().id

        # Modification des informations du producteur
        if not producteur_existe(form_producteur):
                producteur = Productor(name=form_producteur)
                db.session.add(producteur)
        musique.productor_id = Productor.query.filter_by(name=form_producteur).first().id
        
        # Supprimer tout les genres auquel il appartient
        genres_a_supprimer = Appartient.query.filter_by(music_id=musique.id).all()
        for genre in genres_a_supprimer:
                db.session.delete(genre)
        db.session.commit()
        # Modification des genres
        for f_genre in form_genres:
                if genre_existe(f_genre):
                        genre = Genre.query.filter_by(name=f_genre).first()
                else:
                        genre = Genre(name=f_genre)
                        db.session.add(genre)
                        db.session.commit()
                asso = Appartient(music_id=musique.id, genre_id=genre.id)
                db.session.add(asso)
        db.session.commit()


def artiste_existe(artiseName):
        return Artist.query.filter_by(name=artiseName).first() is not None

def producteur_existe(productorName):
        return Productor.query.filter_by(name=productorName).first() is not None

def genre_existe(genreName):
        return Genre.query.filter_by(name=genreName).first() is not None

#### FILTRE #################
def get_music_filter_by_annee(filtre=None):
        ens = set()
        musiques = []
        requette_annees = Music.query.all()
        for musique in requette_annees:
                a = musique.releaseYear
                ens.add(a)
        annees = list(ens)
        annees.sort()
        if filtre == None:
                filtre = annees[0]
        musiques = Music.query.filter_by(releaseYear=filtre).all()
        return (musiques, annees)


def get_music_filter_by_artiste(filtre=None):
        artistes_id = {"#":[]}
        artistes_name = ["#"]
        musiques = []
        requette_artiste = Artist.query.all()
        for artiste in requette_artiste:
                a = artiste.name[0].upper()
                if not a.isalpha():
                        artistes_id["#"].append(artiste.id)
                else:
                        if a not in artistes_id:
                                artistes_id[a] = [artiste.id]
                        else:
                                artistes_id[a].append(artiste.id)
                        if a not in artistes_name:
                                artistes_name.append(a)
        artistes_name.sort()
        if filtre == None:
                if len(artistes_id["#"]) == 0:
                        del artistes_id["#"]
                        artistes_name.remove("#")
                filtre = artistes_name[0]
        for identifiant in artistes_id[filtre]:
                musiques += Music.query.filter_by(artist_id=identifiant).all()
        return (musiques, artistes_name)


def get_music_filter_by_producteur(filtre=None):
        producteur_id = {"#":[]}
        producteur_name = ["#"]
        musiques = []
        requette_producteur = Productor.query.all()
        for producteur in requette_producteur:
                p = producteur.name[0].upper()
                if not p.isalpha():
                        producteur_id["#"].append(producteur.id)
                else:
                        if p not in producteur_id:
                                producteur_id[p] = [producteur.id]
                        else:
                                producteur_id[p].append(producteur.id)
                        if p not in producteur_name:
                                producteur_name.append(p)
        producteur_name.sort()
        if filtre == None:
                if len(producteur_id["#"]) == 0:
                        del producteur_id["#"]
                        producteur_name.remove("#")
                filtre = producteur_name[0]
        for identifiant in producteur_id[filtre]:
                musiques += Music.query.filter_by(productor_id=identifiant).all()
        return (musiques, producteur_name)


def get_music_filter_by_genre(filtre=None):
        genres_music = {}
        genres_name = []
        requette_genres = Genre.query.all()
        for genre in requette_genres:
                name = genre.name.capitalize()
                if not name in genres_music:
                        genres_music[name] = genre.music
                else:
                        genres_music[name] += genre.music
                if name not in genres_name:
                        genres_name.append(name)
        genres_name.sort()
        if filtre == None:
                
                filtre = genres_name[0]
        musiques = genres_music[filtre]
        return (musiques, genres_name)


def get_music_filter_by_titre(filtre=None):
        titre_id = {"#":[]}
        titre_name = ["#"]
        musiques = []
        requette_titre = Music.query.all()
        for musique in requette_titre:
                t = musique.title[0].upper()
                if not t.isalpha():
                        titre_id["#"].append(musique.id)
                else:
                        if t not in titre_id:
                                titre_id[t] = [musique.id]
                        else:
                                titre_id[t].append(musique.id)
                        if t not in titre_name:
                                titre_name.append(t)
        titre_name.sort()
        if filtre == None:
                if len(titre_id["#"]) == 0:
                        del titre_id["#"]
                        titre_name.remove("#")
                filtre = titre_name[0]
        for identifiant in titre_id[filtre]:
                musiques += Music.query.filter_by(id=identifiant).all()
        return (musiques, titre_name)


#### THEME COLOR ############
def get_theme(username):
        return Theme.query.get(load_user(username).theme_id).name

def get_theme_all():
        return Theme.query.all()

def get_theme_first():
        return Theme.query.first().name
