#! /usr/bin/env python3


#### IMPORT #################
from .app import app, db
import module.views
import module.commands
import module.models
