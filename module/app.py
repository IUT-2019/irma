#! /usr/bin/env python3


#### FLASK ##################
from flask import Flask
from flask_breadcrumbs import Breadcrumbs
from flask_login import LoginManager
app = Flask(__name__)
app.debug = True
Breadcrumbs(app = app)
login_manager = LoginManager(app)
login_manager.init_app(app)
#############################




#### SQLAlchemy #############
from flask_sqlalchemy import SQLAlchemy
import os.path

def mkpath(p):
    return os.path.normpath(os.path.join(os.path.dirname(__file__), p))

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config["SQLALCHEMY_DATABASE_URI"] = ("sqlite:///" + mkpath("../myapp.db"))
app.config["SECRET_KEY"] = "d9ee54d5-f044-49da-9489-c976a146cfaa"
db = SQLAlchemy(app)
#############################




#### Bootstrap ##############
from flask_bootstrap import Bootstrap
Bootstrap(app)
app.config["BOOTSTRAP_SERVE_LOCAL"] = True
#############################
