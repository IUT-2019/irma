#! /usr/bin/env python3


#### IMPORT #################
from module.app import app
from module.models import *

from flask import render_template, request, redirect, url_for, session, flash
from flask_paginate import Pagination, get_page_parameter
from flask_breadcrumbs import Breadcrumbs, register_breadcrumb
from flask_login import login_user, current_user, logout_user

from hashlib import sha256
#############################




#### VARIABLE ###############
appName = "IRMA"
nombre_par_page = 8
#############################




#### ROUTES #################
@app.route("/")
@register_breadcrumb(app, '.', 'Accueil')
def index():
    if current_user.is_anonymous:
        session["theme"] = get_theme_first()
    return render_template(
        "accueil.html",
        pageName = "Accueil",
        appName = appName,
        styleFile = ["accueil.css"],
        couleurTheme = session["theme"]
    )


@app.route("/musique/")
@app.route("/musique/filtre/<string:filtre>/")
@app.route("/musique/filtre/<string:filtre>/<string:onglet>/")
@register_breadcrumb(app, '.musique', 'Musique')
def musique(filtre=None, onglet=None):
    search = False
    q = request.args.get("q")
    if q:
        search = True
    page = request.args.get(get_page_parameter(), type=int, default=1)
    onglets = []
    if filtre == None:
        resultat = get_all()
    elif filtre == "annee":
        resultat, onglets = get_music_filter_by_annee(onglet)
    elif filtre == "artiste":
        resultat, onglets = get_music_filter_by_artiste(onglet)
    elif filtre == "producteur":
        resultat, onglets = get_music_filter_by_producteur(onglet)
    elif filtre == "genre":
        resultat, onglets = get_music_filter_by_genre(onglet)
    else:
        resultat, onglets = get_music_filter_by_titre(onglet)
    
    pagination = Pagination(
        page = page,
        total = len(resultat),
        search = search,
        record_name = "resultat",
        per_page = nombre_par_page,
        css_framework='foundation',
        prev_label = "&larr;",
        next_label = "&rarr;",
        outer_window = 0
    )
    return render_template(
        "musique.html",
        pageName = "Musique",
        appName = appName,
        couleurTheme = session["theme"],
        styleFile = ["musique.css"],
        listeMusique = resultat[nombre_par_page * (page - 1): nombre_par_page * page],
        listeOnglet = onglets,
        filtre = filtre,
        ticket = onglet,
        pagination = pagination
    )


@app.route("/musique/fiche/<int:id>/")
@register_breadcrumb(app, '.musique.fiche', 'Fiche')
def ficheMusique(id):
    musique, genres_name = get_musique_by_id(id)
    return render_template(
        "ficheMusique.html",
        pageName = musique.title,
        appName = appName,
        couleurTheme = session["theme"],
        styleFile = ["ficheMusique.css"],
        titre = musique.title,
        image = musique.img,
        artiste = musique.artist.name,
        producteur = musique.productor.name,
        annee = musique.releaseYear,
        genres = genres_name
    )


@app.route("/login/", methods=('GET', 'POST',))
@register_breadcrumb(app, '.login', 'Login')
def login():
    f = LoginForm()
    if request.method == 'POST':
        if f.validate_on_submit():
            user = f.get_authenticated_user()
            if user :
                login_user(user)
                current_user = user
                session["theme"] = get_theme(user.username)
                return redirect(url_for("index"))
    return render_template(
        "connexion.html",
        pageName = "Login",
        appName = appName,
        couleurTheme = session["theme"],
        styleFile = ["connexion.css"],
        form = f
    )

@app.route("/logout/")
def logout():
    logout_user()
    return redirect(url_for('login'))


@app.route("/signup/", methods=('GET', 'POST',))
@register_breadcrumb(app, '.signup', 'Sign Up')
def signup():
    form = RegistrationForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            username = form.username.data
            if load_user(username) is not None:
                flash('Utilisateur existant !')
            else:
                form.registr()
                return redirect(url_for("login"))
    
    return render_template(
        "inscription.html",
        pageName = "Sign Up",
        appName = appName,
        couleurTheme = session["theme"],
        styleFile = ["inscription.css"],
        form = form
    )


@app.route("/preference/")
@app.route("/preference/<int:id>/")
@register_breadcrumb(app, '.preference', 'Préférence')
def preference(id=None):
    if id is not None:
        session["theme"] = current_user.set_theme(id, current_user.get_id())
    return render_template(
        "preference.html",
        pageName = "Préférence",
        appName = appName,
        listeTheme = get_theme_all(),
        couleurTheme = session["theme"],
        styleFile = ["preference.css"]
    )


@app.route("/admin/musique/")
@app.route("/admin/musique/edit/<string:id>/", methods=["GET", "POST"])
@register_breadcrumb(app, '.adminMusique', 'Administration de musique')
def adminMusique(id="-1"):
    form = EditForm()
    delete = DeleteForm()
    if request.method == "POST":
        if form.validate_on_submit():
            if id == "-1":
                new_musique(form)
            elif id == "-2":
                delete_musique(form)
            else:
                update_musique(form)
    musique, genres = get_musique_by_id(id)
    page = request.args.get(get_page_parameter(), type=int, default=1)
    resultat = get_all()
    pagination = Pagination(
        page = page,
        total = len(resultat),
        record_name = "resultat",
        per_page = nombre_par_page,
        css_framework='foundation',
        prev_label = "&larr;",
        next_label = "&rarr;",
        outer_window = 0
    )
    return render_template(
        "adminMusique.html",
        pageName = "Administration musique",
        appName = appName,
        couleurTheme = session["theme"],
        styleFile = ["musique.css", "connexion.css", "adminMusique.css"],
        scriptFile = ["delete-musique.js"],
        listeMusique = resultat[nombre_par_page * (page - 1): nombre_par_page * page],
        form = form,
        delete = delete,
        EditMusique = musique,
        EditGenre = ", ".join(genres),
        pagination = pagination
    )


@app.route("/admin/utilisateur/")
@app.route("/admin/utilisateur/edit/<string:id>/", methods=["GET", "POST"])
@register_breadcrumb(app, '.adminUser', 'Administration d\'Utilisateur')
def adminUser(id="0"):
    form = EditFormUser()
    delete = DeleteForm()
    if request.method == "POST":
        if form.validate_on_submit():
            if id == "-2":
                delete_user(form)
            else:
                update_user(form)
    names = get_user(id)
    page = request.args.get(get_page_parameter(), type=int, default=1)
    resultat = get_all_user()
    pagination = Pagination(
        page=page,
        total=len(resultat),
        record_name="resultat",
        per_page=nombre_par_page,
        css_framework='foundation',
        prev_label="&larr;",
        next_label="&rarr;",
        outer_window=0
    )
    return render_template(
        "adminUser.html",
        pageName="Administration utilisateur",
        appName=appName,
        couleurTheme=session["theme"],
        styleFile=["musique.css", "connexion.css", "adminUser.css"],
        scriptFile=["delete-user.js"],
        listeUser=resultat[nombre_par_page *
                              (page - 1): nombre_par_page * page],
        form=form,
        delete=delete,
        EditUser=names,
        pagination=pagination
    )
