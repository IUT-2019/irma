# irma

### Année 
2018 / 2019

### Type :
Application web de musique

### Objectif du projet

Dans le cadre d'un projet web fourni par les enseignants de l'IUT informatique d'Orléans, nous devons réaliser une application web de gestion de musique. Pour ce faire, le serveur doit être réalisé en python. C'est pourquoi le projet est réalisé en flask. Il utilise un moteur de génération de templates nommé Jinja. Une base de données y est également présente pour pouvoir récupérer, stocker, traiter et renvoyer les différentes informations sur les musiques et/ou les utilisateurs du site internet. Celle-ci est entièrement réalisé au travers le plugin d'SQLALchemy. D'autres plugins font partie de l'application comme celui utilisé pour avoir le framework CSS Bootstrap 3. 

### Liste des développeurs

- Arnaud ORLAY
- Valentin CIZEAU


### Installation
ouvrir un terminal et entrer:
```bash
~$ git clone https://gitlab.com/IUT-2019/irma.git
~$ cd irma
~/irma$ python3 -m venv venv
```

### Activer le venv
```bash
~/irma$ source venv/bin/activate
~/irma$ pip install -r requirement.txt
```

### Création de la base de données
Le venv doit être activé pour utilister les commandes de `flask`.
```bash
~/irma$ flask loaddb extrait.yml
```

### Lancement de l'application
Le venv doit être activé pour utilister les commandes de `flask`.
```bash
~/irma$ flask run
```